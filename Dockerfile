FROM maven:latest as springcook
WORKDIR /workspace
COPY . /workspace/
RUN mvn package

#Start with base JAVA image
FROM openjdk:8
WORKDIR /app
COPY --from=maven-builder /workspace/target /app
EXPOSE 8081

#Application jar file
ARG JAR_FILE=target/projectjee-0.0.1-SNAPSHOT.jar

#ADD Application's jar file to the container
ADD ${JAR_FILE} projectjee.jar

#RUN 
CMD ["sh", "-c","java -jar *.jar"]