package com.group6.projectjee.services;

import com.group6.projectjee.enums.BaseUnit;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.repositories.IngredientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class IngredientServiceTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IngredientRepository ingredientRepository;
    private IngredientService ingredientService;

    @Before
    public void setUp() {
        this.ingredientService = new IngredientService(this.ingredientRepository);
    }

    @Test
    public void should_find_ingredient() {
        Ingredient ingredient = new Ingredient("testIngredient", BaseUnit.NO_UNIT);
        entityManager.persistAndFlush(ingredient);

        assertThat(ingredient.getName()).isEqualTo(ingredientService.findByName("testIngredient").getName());
    }

}
