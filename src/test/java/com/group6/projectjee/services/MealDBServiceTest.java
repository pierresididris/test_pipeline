package com.group6.projectjee.services;

import com.group6.projectjee.enums.BaseUnit;
import com.group6.projectjee.models.Meal;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.repositories.IngredientRepository;
import com.group6.projectjee.repositories.RecipeIngredientRepository;
import com.group6.projectjee.repositories.RecipeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MealDBServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    private IngredientService ingredientService;
    private RecipeIngredientService recipeIngredientService;
    private RecipeService recipeService;
    private MealDBService mealDBService;
    private RestService restService;
    @Autowired private IngredientRepository ingredientRepository;
    @Autowired private RecipeIngredientRepository recipeIngredientRepository;
    @Autowired private RecipeRepository recipeRepository;

    @Before
    public void setUp() {
        this.restService = new RestService(new RestTemplateBuilder());
        this.ingredientService = new IngredientService(this.ingredientRepository);
        this.recipeIngredientService = new RecipeIngredientService(this.recipeIngredientRepository, this.ingredientService);
        this.recipeService = new RecipeService(this.recipeRepository, this. recipeIngredientService, this.restService);
        mealDBService = new MealDBService(ingredientService, recipeIngredientService, recipeService);
    }

    @Test
    public void should_parse_unit() {
        String measure = "8 grammes";

        BaseUnit unit = mealDBService.parseUnit(measure);

        assertThat(unit).isEqualTo(BaseUnit.G);

    }

    @Test
    public void should_parse_quantity() {
        String quantity = "15 litres";

        Float parsed_quantity = mealDBService.parseQuantity(quantity);

        assertThat(parsed_quantity).isEqualTo(15.0f);
    }

    @Test
    public void should_check_ingredients_and_parse_meal_to_recipe() {
        Meal meal = new Meal(
                "testName",
                "testArea",
                "testCategory",
                "testIngredient1",
                "testIngredient2",
                "testIngredient3",
                "testIngredient4",
                "testIngredient5",
                "testIngredient6",
                "testInstructions",
                "testMealThumb",
                "1 testMeasure1",
                "1 testMeasure2",
                "1 testMeasure3",
                "1 testMeasure4",
                "1 testMeasure5",
                "1 testMeasure6"
                );

        mealDBService.checkIngredients(meal);
        Recipe recipe = mealDBService.toRecipe(meal);
        // assertThat(recipe).isEqualTo(null);
        assertThat(meal.getStrMeal()).isEqualTo(recipe.getName());
    }
}
