package com.group6.projectjee.mock;

import com.group6.projectjee.repositories.IngredientRepository;
import com.group6.projectjee.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;

public class IngredientServiceMock extends IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    public IngredientServiceMock(IngredientRepository ingredientRepository) {
        super(ingredientRepository);
    }

    


}
