package com.group6.projectjee.config;

import org.apache.commons.lang3.ArrayUtils;

public class Routes {
    public static final String[] swaggerRoutes = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    public static final String[] publicRoutes = {
            "/api/test/**"
    };

    public static String[] getPublicRoutes() {
        return ArrayUtils.addAll(swaggerRoutes, publicRoutes);
    }


}
