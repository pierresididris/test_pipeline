package com.group6.projectjee.enums;

public enum BaseUnit {
    G,
    CL,
    KG,
    ML,
    L,
    CUP,
    TEASPOON,
    TABLESPOON,
    NO_UNIT
}
