package com.group6.projectjee.repositories;

import com.group6.projectjee.models.Stock;
import com.group6.projectjee.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends CrudRepository<Stock, Integer> {
    Stock findByUser(User user);
}
