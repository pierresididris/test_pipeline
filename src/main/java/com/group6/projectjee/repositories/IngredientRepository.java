package com.group6.projectjee.repositories;

import com.group6.projectjee.models.Ingredient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Integer> {
    Boolean existsByName(String name);
    Boolean existsById(int id);
    Ingredient findByName(String name);
    Ingredient findById(int id);
}
