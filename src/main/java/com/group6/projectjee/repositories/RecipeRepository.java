package com.group6.projectjee.repositories;

import com.group6.projectjee.models.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Integer> {
    Boolean existsByName(String name);
    Recipe findByName(String name);
    Recipe findById(int id);
}
