package com.group6.projectjee.repositories;

import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Stock;
import com.group6.projectjee.models.StockIngredient;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface StockIngredientRepository extends CrudRepository<StockIngredient, Integer> {
    boolean existsByStock(Stock stock);
    StockIngredient getByStock(Stock stock);

    @Query(value = "Select quantity from StockIngredient where stock_id=:stock and ingredient_id=:ingredient")
    float getQuantity(@Param("ingredient") String ingredient, @Param("stock") String stock);

    @Query(value = "Select minQuantity from StockIngredient where stock_id=:stock and ingredient_id=:ingredient")
    float getMinLimit(@Param("ingredient") String ingredient, @Param("stock") String stock);

    @Query(value = "Select ingredient from StockIngredient where stock_id=:stock")
    List<Ingredient> getAllIngredientId(@Param("stock") String stock);

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set quantity=:newQuantity where stock_id=:stock and ingredient_id=:ingredient")
    void updateQuantity(@Param("ingredient") String ingredient, @Param("stock") String stock, @Param("newQuantity") Float newQuantity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set minQuantity=:newLimit where stock_id=:stock and ingredient_id=:ingredient")
    void updateLimit(@Param("ingredient") String ingredient, @Param("stock") String stock, @Param("newLimit") Float newLimit);
}
