package com.group6.projectjee.repositories;

import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.models.RecipeIngredient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RecipeIngredientRepository extends CrudRepository<RecipeIngredient, Integer> {
    ArrayList<RecipeIngredient> findAllByRecipe(Recipe recipe);
    ArrayList<RecipeIngredient> findAllByIngredient(Ingredient ingredient);

    @Query(value="SELECT quantity from RecipeIngredient where ingredient_id=:ingredient and recipe_id=:recipe")
    float getQuantityByRecipeAndIngredient(@Param("ingredient") int ingredient_id, @Param("recipe") int recipe_id);
}
