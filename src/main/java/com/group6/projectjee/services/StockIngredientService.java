package com.group6.projectjee.services;

import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Stock;
import com.group6.projectjee.models.StockIngredient;
import com.group6.projectjee.repositories.StockIngredientRepository;
import org.springframework.aop.AopInvocationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockIngredientService {

    private final StockIngredientRepository stockIngredientRepository;
    private final IngredientService ingredientService;

    public StockIngredientService(StockIngredientRepository stockIngredientRepository, IngredientService ingredientService) {
        this.stockIngredientRepository = stockIngredientRepository;
        this.ingredientService = ingredientService;
    }

    public StockIngredient create(StockIngredient stockIngredient) {
        return stockIngredientRepository.save(stockIngredient);
    }

    public StockIngredient getByStock(Stock stock){
        return stockIngredientRepository.getByStock(stock);
    }

    public Boolean existsByStock(Stock stock){
        return this.stockIngredientRepository.existsByStock(stock);
    }
 
    public void addIngredientQuantity(Ingredient ingredient, float quantity, Stock stock){
        try {
            this.stockIngredientRepository.updateQuantity(String.valueOf(ingredient.getId()), String.valueOf(stock.getId()), quantity + this.getQuantity(ingredient, stock));
        }catch (NullPointerException e){
            //L'ingredient n'existe pas
            System.out.println("Ingredient inexistant");
        }catch (AopInvocationException ex){
            //Le stock n'a pas l'ingredient
            System.out.println("Ingredient pas trouve");
        }
    }

    public Iterable<StockIngredient> getAllIngredients(){
        return this.stockIngredientRepository.findAll();
    }

    public float getQuantity(Ingredient ingredient, Stock stock) {
        try {
            return this.stockIngredientRepository.getQuantity(String.valueOf(ingredient.getId()), String.valueOf(stock.getId()));
        }catch (NullPointerException e){
            //L'ingredient n'existe pas
            return -1;
        }catch (AopInvocationException ex){
            //Le stock n'a pas l'ingredient
            return -2;
        }
    }

    public void updateQuantity(Ingredient ingredient, Stock stock, float quantity) {
        try {
            this.stockIngredientRepository.updateQuantity(String.valueOf(ingredient.getId()), String.valueOf(stock.getId()), quantity);
        }catch (NullPointerException e){
            //L'ingredient n'existe pas
            System.out.println("Ingredient inexistant");
        }catch (AopInvocationException ex){
            //Le stock n'a pas l'ingredient
            System.out.println("Ingredient pas trouve");
        }
    }

    public void updateLimit(Ingredient ingredient, Stock stock, float quantity) {
        try {
            this.stockIngredientRepository.updateLimit(String.valueOf(ingredient.getId()), String.valueOf(stock.getId()), quantity);
        }catch (NullPointerException e){
            //L'ingredient n'existe pas
            System.out.println("Ingredient inexistant");
        }catch (AopInvocationException ex){
            //Le stock n'a pas l'ingredient
            System.out.println("Ingredient pas trouve");
        }
    }

    public float getLimit(Ingredient ingredient, Stock stock) {
        try {
            return this.stockIngredientRepository.getMinLimit(String.valueOf(ingredient.getId()), String.valueOf(stock.getId()));
        }catch (NullPointerException e){
            //L'ingredient n'existe pas
            System.out.println("Ingredient inexistant");
            return -1;
        }catch (AopInvocationException ex){
            //Le stock n'a pas l'ingredient
            System.out.println("Ingredient pas trouve");
            return -2;
        }
    }

    public ArrayList<Ingredient> getAllIngredientFromStock(Stock stock){
        List<Ingredient> id = this.stockIngredientRepository.getAllIngredientId(String.valueOf(stock.getId()));
        ArrayList<Ingredient> res = new ArrayList<>();
        for (Ingredient ingredient : id){
            res.add(ingredient);
        }
        return res;
    }
}
