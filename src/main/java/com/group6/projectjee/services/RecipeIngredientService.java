package com.group6.projectjee.services;

import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.models.RecipeIngredient;
import com.group6.projectjee.repositories.RecipeIngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.logging.Logger;

@Service
public class RecipeIngredientService {

    private final RecipeIngredientRepository recipeIngredientRepository;
    private final IngredientService ingredientService;
    private final Logger logger;

    @Autowired
    public RecipeIngredientService(RecipeIngredientRepository recipeIngredientRepository, IngredientService ingredientService) {
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.ingredientService = ingredientService;
        this.logger = Logger.getLogger("RecipeIngredientService");
    }

    public RecipeIngredient create(RecipeIngredient recipeIngredient) { return this.recipeIngredientRepository.save(recipeIngredient); }

    public ArrayList<Ingredient> getRecipeIngredients(Recipe recipe) {
        ArrayList<RecipeIngredient> results = recipeIngredientRepository.findAllByRecipe(recipe);
        //logger.warning("DEBUG DEBUG DEBUG : " + recipe.toString());
        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
        for (RecipeIngredient entry :
                results) {
            ingredients.add(ingredientService.findById(entry.getIngredient().getId()));
        }
        return ingredients;
    }

    public float getIngredientQuantity(Recipe recipe, Ingredient ingredient){
        return this.recipeIngredientRepository.getQuantityByRecipeAndIngredient(ingredient.getId(), recipe.getId());
    }

}
