package com.group6.projectjee.services;

import com.group6.projectjee.models.*;
import com.group6.projectjee.repositories.StockRepository;
import com.group6.projectjee.repositories.UserRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class StockService {

    private final UserRepository userRepository;
    private final StockRepository stockRepository;
    private final StockIngredientService stockIngredientService;
    private final IngredientService ingredientService;
    private final RecipeService recipeService;
    private final RecipeIngredientService recipeIngredientService;

    public StockService(UserRepository userRepository, StockRepository stockRepository, StockIngredientService stockIngredientService, IngredientService ingredientService, RecipeService recipeService, RecipeIngredientService recipeIngredientService) {
        this.userRepository = userRepository;
        this.stockRepository = stockRepository;
        this.stockIngredientService = stockIngredientService;
        this.ingredientService = ingredientService;
        this.recipeService = recipeService;
        this.recipeIngredientService = recipeIngredientService;
    }

    public ArrayList<Ingredient> getUserIngredients(long userId){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            return this.stockIngredientService.getAllIngredientFromStock(stock);
        } else {
            System.out.println("User inexistant");
            return null;
        }
    }

    public float getUserIngredientQuantity(long userId, int ingredientId){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            Ingredient ingredient = this.ingredientService.findById(ingredientId);
            return this.stockIngredientService.getQuantity(ingredient, stock);
        } else {
            System.out.println("User inexistant");
            return -1;
        }
    }

    public void addIngredientQuantity(long userId, int ingredientId, float quantity){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            Ingredient ingredient = this.ingredientService.findById(ingredientId);
            if(!this.stockIngredientService.existsByStock(stock)){
                StockIngredient stockIngredient = new StockIngredient();
                this.stockIngredientService.create(stockIngredient);
            }
            this.stockIngredientService.addIngredientQuantity(ingredient, quantity, stock);
        } else {
            System.out.println("User inexistant");
        }
    }

    public void updateIngredientQuantity(long userId, int ingredientId, float quantity){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            Ingredient ingredient = this.ingredientService.findById(ingredientId);
            this.stockIngredientService.updateQuantity(ingredient, stock, quantity);
        } else {
            System.out.println("User inexistant");
        }
    }

    public void updateIngredientLimit(long userId, int ingredientId, float quantity){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            Ingredient ingredient = this.ingredientService.findById(ingredientId);
            this.stockIngredientService.updateLimit(ingredient, stock, quantity);
        } else {
            System.out.println("User inexistant");
        }
    }

    public float getIngredientLimit(long userId, int ingredientId){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Stock stock = this.stockRepository.findByUser(user.get());
            Ingredient ingredient = this.ingredientService.findById(ingredientId);
            return this.stockIngredientService.getLimit(ingredient, stock);
        } else {
            System.out.println("User inexistant");
            return -3;
        }
    }

    public void recalculate(Long userId, int recipeId){
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()){
            Recipe recipe = this.recipeService.findRecipeById(recipeId);
            ArrayList<Ingredient> ingredients = recipeIngredientService.getRecipeIngredients(recipe);
            for(Ingredient ingredient : ingredients){
                this.updateIngredientQuantity(userId,
                        ingredient.getId(),
                        this.getUserIngredientQuantity(userId, ingredient.getId()) - recipeIngredientService.getIngredientQuantity(recipe, ingredient) );
            }
        } else {
            System.out.println("User inexistant");
        }
    }

    public boolean availableRecipe(Long userId, Recipe recipe){
        ArrayList<Ingredient> ingredients = recipeIngredientService.getRecipeIngredients(recipe);
        for(Ingredient ingredient : ingredients){
            float neededQuantity = recipeIngredientService.getIngredientQuantity(recipe, ingredient);
            float availableQuantity = this.getUserIngredientQuantity(userId, recipe.getId());
            if(neededQuantity > availableQuantity) return false;
        }
        return true;
    }

    public ArrayList<Recipe> getAllAvailableRecipes(Long userId){
        ArrayList<Recipe> res = new ArrayList<>();
        Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()) {
            Iterable<Recipe> allRecipes = this.recipeService.getAllRecipe();
            for(Recipe recipe : allRecipes){
                if(this.availableRecipe(userId, recipe))res.add(recipe);
            }
            return res;
        } else {
            System.out.println("User inexistant");
            return null;
        }
    }

    /*
    //TODO
    //Replace to and from email
    public void sendEmailToUser(){
        String to = "petit.marcalain@yahoo.fr";
        String from = "13Hiroki13@gmail.com";
        String host = "smtp.gmail.com";
        Properties properties = System.getProperties();
        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                //Put google credentials here !
                return new PasswordAuthentication("", "");

            }

        });
        // Used to debug SMTP issues
        session.setDebug(true);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Notification de stock");

            // Now set the actual message
            message.setText("Nous vous annonçons que votre stock de {placeholder} passe sous la limite !");

            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
    */
}
