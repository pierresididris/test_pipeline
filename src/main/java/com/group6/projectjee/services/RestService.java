package com.group6.projectjee.services;

import com.google.gson.Gson;
import com.group6.projectjee.models.FoodStoreProduct;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.RecipeMealDB;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

@Service
public class RestService {

    private final RestTemplate restTemplate;
    private final Gson g;
    private final Logger logger;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.g = new Gson();
        logger = Logger.getLogger("RestService");
    }

    public RecipeMealDB getRecipe(String url) {
        return g.fromJson(this.restTemplate.getForObject(url, String.class), RecipeMealDB.class);
    }

    public FoodStoreProduct[] getIngredientPrice(Ingredient ingredient) throws HttpServerErrorException.ServiceUnavailable {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-rapidapi-host", "foodstore.p.rapidapi.com");
        headers.add("x-rapidapi-key", System.getenv("RAPIDAPI_KEY"));

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        // logger.info(entity.getBody());
        // logger.info("going to ask https://foodstore.p.rapidapi.com/products?name=" + ingredient.getName());
        HttpEntity<String> responseEntity;
        try {
             responseEntity = this.restTemplate.exchange("https://foodstore.p.rapidapi.com/products?name=" + ingredient.getName(), HttpMethod.GET, entity, String.class);
        } catch (HttpServerErrorException ex) {
            logger.warning(ex.getMessage());
            return new FoodStoreProduct[0];
        }
        if(!responseEntity.hasBody()) {
            logger.warning("HttpRequest failed : responseEntity doesnt have a body");
        }
        return g.fromJson(responseEntity.getBody(), FoodStoreProduct[].class);
    }
}
