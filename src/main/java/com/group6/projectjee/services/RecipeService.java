package com.group6.projectjee.services;

import com.google.gson.Gson;
import com.group6.projectjee.exceptions.RecipeNotFoundException;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.repositories.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeIngredientService recipeIngredientService;
    private final RestService restService;
    private final Logger logger;
    private final Gson g;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository, RecipeIngredientService recipeIngredientService, RestService restService){
        this.recipeRepository = recipeRepository;
        this.recipeIngredientService = recipeIngredientService;
        this.restService = restService;
        this.g = new Gson();
        this.logger = Logger.getLogger("RecipeService");
    }

    public Recipe create(Recipe recipe){
        return this.recipeRepository.save(recipe);
    }

    public Iterable<Recipe> getAllRecipe(){
        return this.recipeRepository.findAll();
    }

    public Recipe findRecipeByName(String name){
        if(this.recipeRepository.existsByName(name))
            return this.recipeRepository.findByName(name);
        else {
            throw new RecipeNotFoundException(name);
//            return null;
        }
    }

    public Recipe findRecipeById(int id) {
        if (existsById(id)){
            return this.recipeRepository.findById(id);
        } else {
            throw new RecipeNotFoundException(id);
        }
    }

    public Boolean existsById(int id) {
        if( this.recipeRepository.existsById(id)){
            return this.recipeRepository.existsById(id);
        } else {
            throw new RecipeNotFoundException(id);
        }
    }

    public Recipe getRandomRecipe(){
        int id = (int) (Math.random()*(this.recipeRepository.count()));
        if(this.recipeRepository.existsById(id)) return this.recipeRepository.findById(id);else return null;
    }

    public ArrayList<Ingredient> getRecipeIngredients(Recipe recipe){
        ArrayList<Ingredient> res = new ArrayList<>();
        return res;
    }

    public String getIngredientsPrice(int recipe_id) {
        if(!existsById(recipe_id)) {
            return "No recipe found with id " + recipe_id;
        }
        Recipe recipe = findRecipeById(recipe_id);
        ArrayList<Ingredient> ingredients = recipeIngredientService.getRecipeIngredients(recipe);
        Float sum = 0.0f;
        for (Ingredient ingredient:
                ingredients) {
            if(restService.getIngredientPrice(ingredient).length == 0) {
                logger.warning("Couldn't get ingredient price for " + ingredient.getName() + ", FoodStore API is probably down");
                return "Couldn't get ingredient price for " + ingredient.getName() + ", FoodStore API is probably down";
            }
            sum += restService.getIngredientPrice(ingredient)[0].getPrice();
        }
        Map<String, String> result = new HashMap<String, String>();
        result.put("recipeName", recipe.getName());
        result.put("estimatedPrice", sum.toString());
        return g.toJson(result);
    }
}
