package com.group6.projectjee.services;

import com.group6.projectjee.enums.BaseUnit;
import com.group6.projectjee.exceptions.IngredientNotFoundException;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Meal;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.models.RecipeIngredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class MealDBService {

    private final IngredientService ingredientService;
    private final RecipeIngredientService recipeIngredientService;
    private final RecipeService recipeService;
    private final Logger logger;

    @Autowired
    public MealDBService(IngredientService ingredientService, RecipeIngredientService recipeIngredientService, RecipeService recipeService) {
        this.ingredientService = ingredientService;
        this.recipeIngredientService = recipeIngredientService;
        this.recipeService = recipeService;
        this.logger = Logger.getLogger("MealDBService");
    }

    public void checkIngredients(Meal meal) {
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();

        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            if(!ingredientService.existsByName(entry.getKey())) {
                BaseUnit unit = this.parseUnit(entry.getValue());
                ingredientService.create(new Ingredient((String) entry.getKey(), unit));
            }
        }
    }

    public BaseUnit parseUnit(String measure) {
        if(measure.matches("[0-9]+ ?g(rammes?)?")) {
            return BaseUnit.G;
        } else if(measure.matches("[0-9]+ ?(cl|centilitres?)")) {
            return BaseUnit.CL;
        } else if(measure.matches("[0-9]+ ?(kg|kilogrammes?)")) {
            return BaseUnit.KG;
        } else if(measure.matches("[0-9]+ ?(ml|millilitres?)")) {
            return BaseUnit.ML;
        } else if(measure.matches("[0-9]+ ?[lL](itres?)?")) {
            return BaseUnit.L;
        } else if(measure.matches("([uU]ne? )?([cC]oupe|[Tt]asse|[Gg]obelet)")) {
            return BaseUnit.CUP;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à café.*")) {
            return BaseUnit.TEASPOON;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à soupe.*")) {
            return BaseUnit.TABLESPOON;
        }
        return BaseUnit.NO_UNIT;
    }

    public Recipe toRecipe(Meal meal) {
        Recipe recipe = recipeService.create(new Recipe(meal.getStrMeal()));
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();

        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            //logger.info("getKey() = " + entry.getKey());
            //logger.info("ingredient for " + recipe.getName() + " : " + ingredientService.findByName(entry.getKey()).getName());
            //recipeIngredientService.create(new RecipeIngredient(recipe, ingredientService.findByName(entry.getKey()), Float.parseFloat(entry.getValue().split(" ")[0].replaceAll("[^0-9]", "")==""?"1":entry.getValue().split(" ")[0].replaceAll("[^0-9]", ""))));
            recipeIngredientService.create(new RecipeIngredient(recipe, ingredientService.findByName(entry.getKey()), this.parseQuantity(entry.getValue())));
        }
        recipeService.create(recipe);
        return recipe;
    }

    public Float parseQuantity(String quantity) {
        if(quantity.matches("([1-9]/[2-9])|½")) {
            String[] calc = quantity.split("/");
            if(Float.parseFloat(calc[1]) == 0){
                return Float.parseFloat(calc[0]);
            }
            return Float.parseFloat(calc[0])/Float.parseFloat(calc[1]);
        }
        quantity = quantity.split(" ")[0];
        if(quantity.replaceAll("[^0-9]", "").equals("")) {
            return 1.0f;
        }

        return Float.parseFloat(quantity.replaceAll("[^0-9]", ""));
    }


}
