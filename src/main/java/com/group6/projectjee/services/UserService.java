package com.group6.projectjee.services;


import com.group6.projectjee.models.User;
import com.group6.projectjee.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;


    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public void Create(User user){
        this.userRepository.save(user);
    }

    public void Delete(User user){
        this.userRepository.delete(user);
    }

    public Iterable<User> getAllUsers()
    {
        return this.userRepository.findAll();
    }

    public Optional<User> findUserById(int userId){
        if(this.userRepository.existsById((long) userId))
            return this.userRepository.findById((long) userId);
        else return null;
    }

}
