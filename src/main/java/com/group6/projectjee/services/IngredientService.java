package com.group6.projectjee.services;

import com.group6.projectjee.exceptions.IngredientNotFoundException;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.repositories.IngredientRepository;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class    IngredientService {

    private final IngredientRepository ingredientRepository;
    private final Logger logger;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
        this.logger = Logger.getLogger("IngredientService");
    }

    public Ingredient create(Ingredient ingredient){

        return this.ingredientRepository.save(ingredient);
    }

    public Iterable<Ingredient> getAllIngredients(){

        return this.ingredientRepository.findAll();
    }

    public Boolean existsByName(String name) {
        return this.ingredientRepository.existsByName(name);
    }

    public Boolean existsById(int id) {
        if (this.ingredientRepository.existsById(id)) {
            return this.ingredientRepository.existsById(id);
        } else {
            throw new IngredientNotFoundException(id);
        }
    }

    public Ingredient findByName(String name) {
        Ingredient ingredient = this.ingredientRepository.findByName(name);
        if(ingredient == null) throw new IngredientNotFoundException(name);
        return ingredient;
    }

    public Ingredient findById(int id){
        if(existsById(id)) {
            return this.ingredientRepository.findById(id);
        } else {
            logger.warning("findById() : Ingredient with id " + id + " doesnt exists");
            return null;
        }

    }
}
