package com.group6.projectjee.exceptions;

public class RecipeNotFoundException  extends RuntimeException{
    public RecipeNotFoundException(String name) {
        super(String.format("recipe Not Found with name %s", name));
    }

    public RecipeNotFoundException(int id) {
        super(String.format("recipe Not Found with name %d", id));
    }
}
