package com.group6.projectjee.exceptions;

public class IngredientNotFoundException extends RuntimeException {
    public IngredientNotFoundException(String name) {
        super(String.format("Ingredient Not Found with name %s", name));
    }

    public IngredientNotFoundException(int id) {
        super(String.format("Ingredient Not Found with id %d", id));
    }
}
