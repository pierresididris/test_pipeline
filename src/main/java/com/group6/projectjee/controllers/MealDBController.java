package com.group6.projectjee.controllers;

import com.google.gson.Gson;
import com.group6.projectjee.exceptions.IngredientNotFoundException;
import com.group6.projectjee.models.RecipeMealDB;
import com.group6.projectjee.services.MealDBService;
import com.group6.projectjee.services.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/mealdb", produces = MediaType.APPLICATION_JSON_VALUE)
public class MealDBController {

    private final RestService restService;
    private final MealDBService mealDBService;
    private final Gson g;
    private final Logger logger;

    @Autowired
    public MealDBController(RestService restService, MealDBService mealDBService) {
        this.restService = restService;
        this.mealDBService = mealDBService;
        this.g = new Gson();
        logger = Logger.getLogger("MealDBController");
    }

    @GetMapping
    public String getRecipe() throws IngredientNotFoundException {
        RecipeMealDB recipe = restService.getRecipe("https://www.themealdb.com/api/json/v1/1/random.php");
        recipe.meals.get(0).toFrench();
        mealDBService.checkIngredients(recipe.meals.get(0));
        mealDBService.toRecipe(recipe.meals.get(0));
        return g.toJson(recipe);
    }
}