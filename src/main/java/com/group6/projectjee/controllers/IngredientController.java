package com.group6.projectjee.controllers;

import com.group6.projectjee.exceptions.IngredientNotFoundException;
import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.services.IngredientService;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/ingredient")
public class IngredientController {

    private final IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Ingredient create(@RequestBody Ingredient ingredient) {
        this.ingredientService.create(ingredient);
        return ingredient;
    }

    @GetMapping("/all")
    @NotFound
    public Iterable<Ingredient> getIngredients() { return this.ingredientService.getAllIngredients();}

    @GetMapping("/{name}")
    @NotFound
    public Boolean getIngredientByName(@PathVariable String name) {
        return this.ingredientService.existsByName(name);
    }
//    public Ingredient getIngredientByName(@PathVariable String name) throws IngredientNotFoundException {
//        return this.ingredientService.findByName(name);
//    }


}
