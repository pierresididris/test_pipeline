package com.group6.projectjee.controllers;

import com.google.gson.Gson;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.services.IngredientService;
import com.group6.projectjee.services.RecipeIngredientService;
import com.group6.projectjee.services.RecipeService;
import com.group6.projectjee.services.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/recipe")
public class RecipeController {

    private final RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService, RestService restService, Gson g, IngredientService ingredientService, RecipeIngredientService recipeIngredientService) {
        this.recipeService = recipeService;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Recipe create(@RequestBody Recipe recipe) {
        this.recipeService.create(recipe);
        return recipe;
    }

    @GetMapping("/all")
    public Iterable<Recipe> getRecipes() { return this.recipeService.getAllRecipe();}

    //Find RecipeByName
    @GetMapping("/{name}")
    public Recipe getByName(@PathVariable String name){
        return this.recipeService.findRecipeByName(name);
    }

    //TODO
    //Find a random recipe
    @GetMapping("/randomRecipe")
    public Recipe getRandomRecipe(){
        return this.recipeService.getRandomRecipe();
    }

    //TODO
    //Check if recipe requires ingredient
    @GetMapping("/hasIngredient/{ingredient}")
    public Boolean hasIngredient(@PathVariable String ingredient) {
        return true;
    }

    //TODO
    @GetMapping("/getAllIngredients/{recipe}")
    public void getAllIngredient(@PathVariable Recipe recipe){ }

    @GetMapping("/price/{recipe_id}")
    public String getIngredientPrices(@PathVariable int recipe_id) { return this.recipeService.getIngredientsPrice(recipe_id); }
}
