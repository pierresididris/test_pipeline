package com.group6.projectjee.controllers;

import com.group6.projectjee.models.Ingredient;
import com.group6.projectjee.models.Recipe;
import com.group6.projectjee.models.Stock;
import com.group6.projectjee.repositories.StockRepository;
import com.group6.projectjee.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/stock")
@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
public class StockController {

    private final StockRepository stockRepository;
    private final StockService stockService;

    @Autowired
    public StockController(StockRepository stockRepository, StockService StockService){
        this.stockRepository = stockRepository;
        this.stockService = StockService;
    }

    @GetMapping("/ingredientsList/{userId}")
    public ArrayList<Ingredient> getUserIngredients(@PathVariable String userId){
        return stockService.getUserIngredients(Long.parseLong(userId));
    }

    @GetMapping("/getIngredientQuantity/{userId}/{ingredientId}")
    public float getUserIngredientQuantity(@PathVariable String userId, @PathVariable String ingredientId){
        return this.stockService.getUserIngredientQuantity(Long.parseLong(userId), Integer.parseInt(ingredientId));
    }

    @GetMapping("/addIngredientQuantity/{userId}/{ingredientId}/{quantity}")
    public void addUserIngredientQuantity(@PathVariable String userId, @PathVariable String ingredientId, @PathVariable String quantity){
        this.stockService.addIngredientQuantity(Long.parseLong(userId), Integer.parseInt(ingredientId), Float.parseFloat(quantity));
    }

    @GetMapping("/updateIngredientQuantity/{userId}/{ingredientId}/{quantity}")
    public void updateIngredientQuantity(@PathVariable String userId, @PathVariable String ingredientId, @PathVariable String quantity){
        this.stockService.updateIngredientQuantity(Long.parseLong(userId), Integer.parseInt(ingredientId), Float.parseFloat(quantity));
    }

    @GetMapping("/updateIngredientLimit/{userId}/{ingredientId}/{quantity}")
    public void updateIngredientLimit(@PathVariable String userId, @PathVariable String ingredientId, @PathVariable String quantity){
        this.stockService.updateIngredientLimit(Long.parseLong(userId), Integer.parseInt(ingredientId), Float.parseFloat(quantity));
    }

    @GetMapping("/getIngredientLimit/{userId}/{ingredientId}")
    public float getIngredientLimit(@PathVariable String userId, @PathVariable String ingredientId){
        return this.stockService.getIngredientLimit(Long.parseLong(userId), Integer.parseInt(ingredientId));
    }

    @GetMapping("/recalculate/{userId}/{recipeId}")
    public void recalculateStock(@PathVariable String userId, @PathVariable String recipeId){
        this.stockService.recalculate(Long.parseLong(userId), Integer.parseInt(recipeId));
    }

    @GetMapping("/getAvailableRecipes/{userId}")
    public ArrayList<Recipe> getAvailableRecipes(@PathVariable String userId){
        return this.stockService.getAllAvailableRecipes(Long.parseLong(userId));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Stock create(Stock stock){
        stockRepository.save(stock);
        return stock;
    }

    //Test Purposes
    @GetMapping("/sendEmail")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public void sendEmail(){
        //stockService.sendEmailToUser();
    }

}
