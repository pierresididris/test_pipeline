package com.group6.projectjee.controllers;

import com.group6.projectjee.services.UserService;
import com.group6.projectjee.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public User create(@RequestBody User user) {
        this.userService.Create(user);
        return user;
    }

    @GetMapping("/all")
    public Iterable<User> getUsers() { return this.userService.getAllUsers();}
}
