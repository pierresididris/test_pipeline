package com.group6.projectjee.models;

import java.io.Serializable;

public class RecipeIngredientID implements Serializable {

    private int recipe;
    private int ingredient;

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 17 + recipe;
        hash = hash * 31 + ingredient;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
