package com.group6.projectjee.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.group6.projectjee.enums.BaseUnit;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
//@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Data
@Entity
public class Ingredient {

    @Id
    @GeneratedValue
    private int id;

    @Column(unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private BaseUnit baseUnit;

    @OneToMany(mappedBy = "ingredient")
    @JsonManagedReference
    private List<RecipeIngredient> recipeIngredients;

    @OneToMany(mappedBy = "ingredient")
    @JsonManagedReference
    private List<StockIngredient> stockIngredients;

    public Ingredient(String name, BaseUnit baseUnit) {
        this.name = name;
        this.baseUnit = baseUnit;
    }

    public int getId() {
        return id;
    }
}
