package com.group6.projectjee.models;

import java.io.Serializable;

public class StockIngredientID implements Serializable {

    private Stock stock;
    private Ingredient ingredient;
    private float quantity;

    public StockIngredientID(){

    }

    public StockIngredientID(Ingredient ingredient, float quantity, Stock stock){
        this.stock = stock;
        this.ingredient = ingredient;
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public Stock getStock(){return stock;}

    public Ingredient getIngredient(){return ingredient;}

    public float getQuantity(){return  quantity;}

    public void setIngredient(Ingredient ingredient){this.ingredient = ingredient;}

    public void setStock(Stock stock){ this.stock = stock;}

    public void setQuantity(float quantity){this.quantity = quantity;}
}
