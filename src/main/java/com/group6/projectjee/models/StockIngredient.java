package com.group6.projectjee.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@IdClass(StockIngredientID.class)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"stock_id", "ingredient_id"}))
public class StockIngredient implements Serializable {
    // jointure stock - ingredient

    @Id
    @ManyToOne
    @JoinColumn(name = "stock_id", referencedColumnName = "id")
    @JsonBackReference
    private Stock stock;

    @Id
    @ManyToOne
    @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    @JsonBackReference
    private Ingredient ingredient;

    public Ingredient getIngredient(){
        return this.ingredient;
    }

    @Column(nullable = false)
    private float quantity;

    @Column(nullable = false)
    private float minQuantity;

}
