 package com.group6.projectjee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication()
@EnableJpaAuditing // Pour les données auto-générées des entités ex : createdAt et updatedAt
public class ProjectJeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectJeeApplication.class, args);
    }

}
